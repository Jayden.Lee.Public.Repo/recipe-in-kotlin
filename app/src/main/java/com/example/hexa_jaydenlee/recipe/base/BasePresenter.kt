package com.example.hexa_jaydenlee.recipe.base

import com.example.hexa_jaydenlee.recipe.realm.RecipeRealmHandler
import io.realm.Realm

/**
 * Created by  Jayden Lee on 1/12/2018.
 */

open class BasePresenter< T : BaseView> : BaseInterfacePresenter<T>{

    var recipeHandler = RecipeRealmHandler()
    var realm = Realm.getDefaultInstance()!!
    private var mMvpView: T? = null


    override fun onAttach(view: T) {
        mMvpView = view
    }

    override fun onDetach() {
        mMvpView = null
    }

    open fun getMvpView(): T? {
        return mMvpView
    }

}