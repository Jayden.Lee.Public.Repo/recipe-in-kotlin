package com.example.hexa_jaydenlee.recipe.base

import android.view.View

/**
 * Created by  Jayden Lee on 1/12/2018.
 */
interface BaseInterfacePresenter<in T : BaseView> {
    fun onAttach(view: T)

    fun onDetach()
}