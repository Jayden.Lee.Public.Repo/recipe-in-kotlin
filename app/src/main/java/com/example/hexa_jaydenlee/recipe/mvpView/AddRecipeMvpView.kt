package com.example.hexa_jaydenlee.recipe.mvpView

import android.view.View
import com.example.hexa_jaydenlee.recipe.base.BaseView

/**
 * Created by  Jayden Lee on 1/12/2018.
 */
interface AddRecipeMvpView : BaseView {
    fun showSuccessAdd()
    fun showFailedAdd()
    fun backToPreviousFrag()
}