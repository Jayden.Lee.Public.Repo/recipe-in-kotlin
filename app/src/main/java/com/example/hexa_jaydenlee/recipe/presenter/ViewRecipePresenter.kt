package com.example.hexa_jaydenlee.recipe.presenter

import com.example.hexa_jaydenlee.recipe.base.BasePresenter
import com.example.hexa_jaydenlee.recipe.mvpView.ViewRecipeMvpView
import com.example.hexa_jaydenlee.recipe.realm.Recipe
import timber.log.Timber

/**
 * Created by  Jayden Lee on 1/12/2018.
 */

class ViewRecipePresenter:BasePresenter<ViewRecipeMvpView>(){

    fun updateRecipes(recipeID:Int, recipeName:String, recipeIngredient:String, recipeType:Int) {
        try {
            val updatedRecipe = Recipe(
                    recipeID = recipeID,
                    recipeName = recipeName,
                    recipeIngredient = recipeIngredient,
                    recipeType = recipeType
            )
            if (recipeHandler.editRecipe(realm, updatedRecipe)) {
                getMvpView()!!.showUpdateRecipe()
            }

        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    fun deleteRecipes(recipeID:Int){
            try {
                if(recipeHandler.delRecipe(realm , recipeID)){
                    getMvpView()!!.showDeleteRecipe()
                }
            }catch (e:Exception){
                Timber.e(e)
            }
        }

    fun retrieveRecipeDetail(recipeID: Int){
        val v = recipeHandler.getRecipe(realm ,  recipeID)
        getMvpView()!!.retrieveRecipeDetail(v)
    }
}