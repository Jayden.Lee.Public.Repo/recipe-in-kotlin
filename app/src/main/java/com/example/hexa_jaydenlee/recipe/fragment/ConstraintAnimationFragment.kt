package com.example.hexa_jaydenlee.recipe.fragment

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.transition.ChangeBounds
import android.support.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateOvershootInterpolator
import android.widget.ImageView
import android.widget.TextView

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.base.BaseFragment
import java.io.File
import android.text.method.ScrollingMovementMethod



/**
 * Created by Jayden Lee on 2/23/2018.
 */
class ConstraintAnimationFragment : BaseFragment(){

    private var show = false

    @BindView(R.id.tv_tap_info) lateinit var tvTapInfo: TextView
    @BindView(R.id.cl_before_layout) lateinit var clBeforeLayout: ConstraintLayout
    @BindView(R.id.iv_meme) lateinit var ivMeme: ImageView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.before_layout, container, false)
        ButterKnife.bind(this, view)
//        tempFile = File(context.getExternalFilesDir("img"), "temp.jpg")
//        presenter.onAttach(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvTapInfo.movementMethod = ScrollingMovementMethod()
        ivMeme.setOnClickListener {
            if(show)
                hideComponent() // if the animation is shown, we hide back the views
            else
                showComponent() // if the animation is NOT shown, we animate the views
        }
    }

    fun hideComponent(){

        show = false
        val constraintSet = ConstraintSet()
        constraintSet.clone(context, R.layout.before_layout)

        val transition = ChangeBounds()
        transition.interpolator = AnticipateOvershootInterpolator(1.0f)
        transition.duration = 1200

        TransitionManager.beginDelayedTransition(clBeforeLayout, transition)
        constraintSet.applyTo(clBeforeLayout)
    }


    fun showComponent(){

        show = true
        Log.d("Clicked", "Yes")
        val constraintSet = ConstraintSet()
        constraintSet.clone(context, R.layout.after_layout)

        val transition = ChangeBounds()
        transition.interpolator = AnticipateOvershootInterpolator(1.0f)
        transition.duration = 1200

        TransitionManager.beginDelayedTransition(clBeforeLayout, transition)
        constraintSet.applyTo(clBeforeLayout)
    }

    override fun injectAppComponent() {
        getAppComponent().inject(this)
    }

}