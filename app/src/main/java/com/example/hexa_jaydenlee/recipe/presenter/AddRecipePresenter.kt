package com.example.hexa_jaydenlee.recipe.presenter

import com.example.hexa_jaydenlee.recipe.base.BasePresenter
import com.example.hexa_jaydenlee.recipe.mvpView.AddRecipeMvpView
import com.example.hexa_jaydenlee.recipe.realm.Recipe

/**
 * Created by  Jayden Lee on 1/12/2018.
 */

open class AddRecipePresenter : BasePresenter<AddRecipeMvpView>() {

    fun addRecipe(recipeName:String, recipeIngredient:String , recipeType:Int ){
        if (recipeHandler.getAllRecipe(realm).count() <= 0) {
            val newRecipe = Recipe(
                    recipeID = 1 , // intitial value
                    recipeName = recipeName,
                    recipeIngredient = recipeIngredient,
                    recipeType = recipeType
            )
            recipeHandler.addRecipe(realm, newRecipe)
        } else {
            val v = recipeHandler.getLastRecipeId(realm)
            val newRecipeWithNewID =  v.copy(v.recipeID +1, recipeName, recipeIngredient, recipeType)
            recipeHandler.addRecipe(realm, newRecipeWithNewID)

        }
        getMvpView()!!.showSuccessAdd()
    }
}