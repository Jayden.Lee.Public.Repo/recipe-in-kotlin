package com.example.hexa_jaydenlee.recipe.application

import android.content.Context
import android.content.SharedPreferences
import android.support.annotation.StringDef
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Jayden Lee on 1/23/2018.
 */
@Singleton class SharePreferenceHelper{

    private val CORE_PREF_FILE_NAME:String = "core_pref_file"
    private val mPref:SharedPreferences

    @Inject
    constructor(@ApplicationContext context: Context){
        mPref = context.getSharedPreferences(CORE_PREF_FILE_NAME, Context.MODE_PRIVATE)
    }

    companion object {
        const val B_FLAG = "b_flag"
    }


    fun clear(){
    mPref.edit().clear().apply()
    }


    fun getString(@SharePreferenceHelper.Key key:String, defValue:String ): String{
        return mPref.getString(key, defValue)
    }

    fun getInt(@SharePreferenceHelper.Key key:String, defValue:Int ):Int{
        return mPref.getInt(key, defValue)
    }

    fun getBoolean(@Key key:String , defValue:Boolean):Boolean {
        return mPref.getBoolean(key, defValue)
    }

    fun putString(@SharePreferenceHelper.Key key:String , value:String){
        val editor = mPref.edit()
        editor.putString(key , value)
    }

    fun putBoolean(@Key key: String, value: Boolean) {
        val editor = mPref.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun putInt(@SharePreferenceHelper.Key key: String, value: Int) {
        val editor = mPref.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    fun remove(@SharePreferenceHelper.Key key: String) {
        val editor = mPref.edit()
        editor.remove(key).apply()
    }

    @StringDef (B_FLAG)


    @Retention(RetentionPolicy.SOURCE)
    annotation class Key


}