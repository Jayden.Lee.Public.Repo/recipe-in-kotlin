package com.example.hexa_jaydenlee.recipe.fragment

import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnItemSelected
import com.example.hexa_jaydenlee.recipe.DrawerActivity
import com.example.hexa_jaydenlee.recipe.base.BaseFragment
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.adapter.RecycleViewAdapter
import com.example.hexa_jaydenlee.recipe.application.SharePreferenceHelper
import com.example.hexa_jaydenlee.recipe.model.RecipeTypeModel
import com.example.hexa_jaydenlee.recipe.mvpView.MainMenuMvpView
import com.example.hexa_jaydenlee.recipe.presenter.MainMenuPresenter
import com.example.hexa_jaydenlee.recipe.realm.Recipe
import com.example.hexa_jaydenlee.recipe.util.AppUtil
import com.example.hexa_jaydenlee.recipe.util.ParserUtil
import javax.inject.Inject


/**
* Created by Jayden Lee on 1/5/2018.
*/

class MainMenuMvpFragment : BaseFragment(), MainMenuMvpView {

    //Declaration
    @BindView(R.id.btn_add_recipe) lateinit var btnAddRecipe: Button
    @BindView(R.id.spinner1) lateinit var spnRecipeType: Spinner
    @BindView(R.id.cl_empty_value) lateinit var clEmptyValue: ConstraintLayout

    @Inject lateinit var mAppUtil: AppUtil
    @Inject lateinit var mSharePreferences : SharePreferenceHelper

    private val presenter:MainMenuPresenter = MainMenuPresenter()
    companion object {
        fun newInstance(): MainMenuMvpFragment {
            return MainMenuMvpFragment()
        }
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_main_menu, container, false)
        ButterKnife.bind(this, view)
        presenter.onAttach(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Parse the XML value into Spinner
        val parser = ParserUtil()
        val recipeTypeList = parser.parse(resources.openRawResource(R.raw.recipetypes),true)
        val spinnerAdapter: ArrayAdapter<RecipeTypeModel> = ArrayAdapter(activity, R.layout.support_simple_spinner_dropdown_item, recipeTypeList)
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spnRecipeType.adapter = spinnerAdapter

        presenter.showRecipeListValue(0)
        mSharePreferences.putBoolean(SharePreferenceHelper.B_FLAG , true)

    }

    override fun onResume() {
        super.onResume()
        presenter.showRecipeListValue(0)
    }

    @OnClick(R.id.btn_add_recipe)
    fun addRecipe() {
        val addRecipe = AddRecipeMvpFragment.newInstance()
        switchFragment(addRecipe)
    }

    @OnItemSelected(R.id.spinner1)
    fun filterRecipeList(position: Int) {
        presenter.showRecipeListValue(position)
    }


    // Filter the value by categories
    override fun onAttach() {}

    override fun showCurrentRecipeValue(recipeType: Int, allRealmResult: List<Recipe>) {
        spnRecipeType.setSelection(recipeType)

        val recyclerView = activity.findViewById(R.id.rv_recipe) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        val adapter = RecycleViewAdapter(allRealmResult)
        clEmptyValue.visibility = if (allRealmResult.isEmpty()) View.VISIBLE else View.GONE
        recyclerView.adapter = adapter
    }

    override fun injectAppComponent() {
        getAppComponent().inject(this)
    }

    @OnClick(R.id.btn_switch_mode)
    fun switchToTestPlugin(){
        val intent =Intent(context, DrawerActivity::class.java)
        startActivity(intent)

//        val cropPhoto = CropPhotoFragment.newInstance()
//        switchFragment(cropPhoto)
    }

}