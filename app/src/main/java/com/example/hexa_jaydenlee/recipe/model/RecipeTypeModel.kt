package com.example.hexa_jaydenlee.recipe.model

/**
 * Created by  Jayden Lee on 1/8/2018.
 */

//open class RecipeTypeModel(
//    open var recipeName : String = "",
//    open var recipeIngredient: String = "",
//    open var recipeType: Int = 0
//){
//    fun getterSetter
//}


class RecipeTypeModel{
    var name: String = ""
    var _id: Int = 0

    fun copyRecipe (recipe: RecipeTypeModel) : RecipeTypeModel{
        val result = RecipeTypeModel()
        result._id = recipe._id
        result.name = recipe.name
        return result
    }

    override fun toString(): String {
        return name
    }
}