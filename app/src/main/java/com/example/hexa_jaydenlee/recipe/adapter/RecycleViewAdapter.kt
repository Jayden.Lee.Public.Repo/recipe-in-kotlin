package com.example.hexa_jaydenlee.recipe.adapter

import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.realm.Recipe


/**
 * Created by  Jayden Lee on 1/6/2018.
 */

//Adapter for the main menu recyclerView
class RecycleViewAdapter(private val recipeList: List<Recipe>) : RecyclerView.Adapter<ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(recipeList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =LayoutInflater.from(parent.context).inflate(R.layout.list_view, parent , false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return recipeList.size
    }
}