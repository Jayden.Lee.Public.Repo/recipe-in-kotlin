package com.example.hexa_jaydenlee.recipe.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import butterknife.ButterKnife
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.fragment.MainMenuMvpFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        supportFragmentManager
                .beginTransaction()
                .add(R.id.frm_layout, MainMenuMvpFragment.newInstance(), "MainMenu")
                .commit()
    }

}
