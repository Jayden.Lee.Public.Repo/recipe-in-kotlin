package com.example.hexa_jaydenlee.recipe.realm

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by  Jayden Lee on 1/5/2018.
 */

open class Recipe (
        @PrimaryKey open var  recipeID: Int = 0,
        open var recipeName : String = "",
        open var recipeIngredient: String = "",
        open var recipeType: Int = 0
) :  RealmObject (){
    fun copy(
            recipeID: Int = this.recipeID,
            recipeName: String = this.recipeName,
            recipeIngredient: String = this.recipeIngredient,
            recipeType: Int = this.recipeType)
            = Recipe (recipeID , recipeName , recipeIngredient, recipeType)
}

