package com.example.hexa_jaydenlee.recipe.application

import android.app.Application
import android.content.Context
import com.example.hexa_jaydenlee.recipe.util.AppUtil
import dagger.android.DaggerApplication
import dagger.android.DaggerContentProvider
import dagger.android.support.DaggerAppCompatActivity
import dagger.internal.DaggerCollections
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Inject



/**
 * Created by  Jayden Lee on 1/12/2018.
 */

class App : Application(){


    private val component:AppComponent =
            DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()


    companion object {
        fun getAppComponent(mContext: Context): AppComponent {
            val app = mContext.applicationContext as App
            return app.component
        }
    }


    override fun onCreate() {
        super.onCreate()
        Realm.init(this)

        val configuration : RealmConfiguration
                = RealmConfiguration.Builder()
                    .name("sample.realm")
                    .schemaVersion(1).build()

        Realm.setDefaultConfiguration(configuration)
        Realm.getInstance(configuration)

        component.inject(this)
    }

    override fun onTerminate() {
        Realm.getDefaultInstance().close()
        super.onTerminate()
    }
}