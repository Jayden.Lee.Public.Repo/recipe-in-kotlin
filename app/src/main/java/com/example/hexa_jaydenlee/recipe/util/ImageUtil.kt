package com.example.hexa_jaydenlee.recipe.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import timber.log.Timber
import java.io.File

/**
 * Created by Jayden Lee on 1/18/2018.
 */

class ImageUtil{

    fun decodeSampledBitmapFromFile(file: File, reqWidth: Int, reqHeight: Int): Bitmap? {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(file.absolutePath, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false

        // Increase inSampleSize when OutOfMemoryError
        var retry = 0
        var mBitmap: Bitmap? = null
        while (retry <= 2) {
            try {
                mBitmap = BitmapFactory.decodeFile(file.absolutePath, options)
                break
            } catch (e: OutOfMemoryError) {
                Timber.e(e, "out of memory, options.inSampleSize=[%s]", options.inSampleSize)
                options.inSampleSize = options.inSampleSize + 1 //down by 1
            }

            retry++
        }
        return mBitmap
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        var height = options.outHeight
        var width = options.outWidth
        if (width < height) {
            // Portrait mode
            height = options.outWidth
            width = options.outHeight
        }
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val halfHeight = height / 2
            val halfWidth = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }


    fun imageRotateSettings(checkImagePath: String): Bitmap? {
        var bitmap: Bitmap? = null
        try {
            val file = File(checkImagePath)
            bitmap = decodeSampledBitmapFromFile(file, 200, 200)

            val exif = ExifInterface(checkImagePath)
            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90f)
            } else if (orientation == 3) {
                matrix.postRotate(180f)
            } else if (orientation == 8) {
                matrix.postRotate(270f)
            }
            bitmap = Bitmap.createBitmap(bitmap!!, 0, 0, bitmap.width, bitmap.height, matrix, true) // rotating bitmap
        } catch (e: Exception) {
            Timber.e(e, "Error while imageRotateSettings(), checkImagePath = %s", checkImagePath)
        }

        return bitmap
    }

}