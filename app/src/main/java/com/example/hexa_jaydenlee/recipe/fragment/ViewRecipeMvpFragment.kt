package com.example.hexa_jaydenlee.recipe.fragment


import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.hexa_jaydenlee.recipe.base.BaseFragment
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.model.RecipeTypeModel
import com.example.hexa_jaydenlee.recipe.mvpView.ViewRecipeMvpView
import com.example.hexa_jaydenlee.recipe.presenter.ViewRecipePresenter
import com.example.hexa_jaydenlee.recipe.realm.Recipe
import com.example.hexa_jaydenlee.recipe.util.AppUtil
import com.example.hexa_jaydenlee.recipe.util.ParserUtil
import javax.inject.Inject

/**
 * Created by  Jayden Lee on 1/5/2018.
 */

class ViewRecipeMvpFragment : BaseFragment() , ViewRecipeMvpView {

    //Declaration
    @BindView(R.id.et_recipe_name) lateinit var etRecipeName: EditText
    @BindView(R.id.et_recipe_ingredient) lateinit var etRecipeIngredient: EditText
    @BindView(R.id.spinner1) lateinit var  spnRecipeType : Spinner
    @BindView(R.id.btn_update_recipe) lateinit var btnUpdateRecipe: Button
    @BindView(R.id.btn_delete_recipe) lateinit var btnDeleteRecipe: Button

    @Inject lateinit var mAppUtil: AppUtil
    private var recipeID:Int = 0
    private val presenter: ViewRecipePresenter = ViewRecipePresenter()

    companion object {
        fun newInstance(recipeID:Int): ViewRecipeMvpFragment {
            val args = Bundle()
            val fragment = ViewRecipeMvpFragment()
            args.putInt("recipeID" , recipeID)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recipeID = arguments.getInt("recipeID")
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view:View = inflater!!.inflate(R.layout.fragment_view_recipe, container, false)
        ButterKnife.bind(this , view)
        presenter.onAttach(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.retrieveRecipeDetail(recipeID)

        // Parse the XML value into Spinner
        val parser= ParserUtil()
        val recipeTypeList:List<RecipeTypeModel> = parser.parse(resources.openRawResource(R.raw.recipetypes),false)
        val spinnerAdapter:ArrayAdapter<RecipeTypeModel> = ArrayAdapter(activity, R.layout.support_simple_spinner_dropdown_item, recipeTypeList)
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spnRecipeType.adapter = spinnerAdapter
    }

    override fun onResume() {
        super.onResume()
        val v = recipeHandler.getRecipe(realm ,  recipeID)
        spnRecipeType.setSelection(v.recipeType)
    }

    @OnClick(R.id.btn_update_recipe)
    fun updateRecipe(){
            presenter.updateRecipes(recipeID ,
                    etRecipeName.text.toString(),
                    etRecipeIngredient.text.toString(),
                    spnRecipeType.selectedItemPosition)
    }

    @OnClick(R.id.btn_delete_recipe)
    fun deleteRecipe(){
        presenter.deleteRecipes(recipeID)
    }

    override fun onAttach() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retrieveRecipeDetail(recipe: Recipe) {
        etRecipeName.text = Editable.Factory.getInstance().newEditable(recipe.recipeName)
        etRecipeIngredient.text = Editable.Factory.getInstance().newEditable(recipe.recipeIngredient)
        spnRecipeType.setSelection(recipe.recipeType)
    }

    override fun showDeleteRecipe() {
        backToPreviousFrag()
        toastValue(resources.getString(R.string.toast_recipe_deleted))
    }

    override fun showUpdateRecipe() {
        backToPreviousFrag()
        toastValue(resources.getString(R.string.toast_recipe_updated))
    }

    override fun injectAppComponent() {
        getAppComponent().inject(this)
    }
}