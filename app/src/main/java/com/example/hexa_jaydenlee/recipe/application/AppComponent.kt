package com.example.hexa_jaydenlee.recipe.application

import android.app.Application
import android.content.Context
import com.example.hexa_jaydenlee.recipe.base.BaseFragment
import com.example.hexa_jaydenlee.recipe.fragment.AddRecipeMvpFragment
import com.example.hexa_jaydenlee.recipe.fragment.MainMenuMvpFragment
import com.example.hexa_jaydenlee.recipe.util.AppUtil
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Jayden Lee on 1/23/2018.
 */
@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent{

    fun inject(frag: BaseFragment)

    fun inject(app: Application)

    fun inject(frag: AddRecipeMvpFragment)

    fun inject(frag: MainMenuMvpFragment)

    fun application(): Application

    @ApplicationContext
    fun context(): Context

    fun appUtil(): AppUtil
}