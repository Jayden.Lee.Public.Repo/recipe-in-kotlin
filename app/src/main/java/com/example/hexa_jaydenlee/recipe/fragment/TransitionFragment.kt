package com.example.hexa_jaydenlee.recipe.fragment

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import butterknife.ButterKnife
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_transition_testing.*
import bg.devlabs.transitioner.*
import butterknife.BindView
import com.example.hexa_jaydenlee.recipe.util.AppUtil
import javax.inject.Inject

/**
 * Created by Jayden Lee on 1/18/2018.
 */
class TransitionFragment : BaseFragment(){

    @BindView(R.id.first_layout) lateinit var clMainLayout:ConstraintLayout
    @BindView(R.id.second_layout) lateinit var clSecondLayout:ConstraintLayout

    @Inject lateinit var mAppUtil: AppUtil

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_transition_testing, container, false)
        ButterKnife.bind(this, view)

//        presenter.onAttach(this)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val transition = Transitioner(clMainLayout, clSecondLayout)
        btn_switch_transition.setOnClickListener {
            transition.setProgress(90)
        }
//        transition.duration = 500
//
//        transition.interpolator = AccelerateDecelerateInterpolator()
//
//        transition.animateTo(percent = 0f)
//
//        transition.onProgressChanged {
//            //triggered on every progress change of the transition
//            seekBar.progress = (it * 100).toInt()
//        }
//
//        val progress: Float = transition.currentProgress
    }

    override fun injectAppComponent() {
        getAppComponent().inject(this)
    }
}