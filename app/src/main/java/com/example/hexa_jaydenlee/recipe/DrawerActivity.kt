package com.example.hexa_jaydenlee.recipe

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.example.hexa_jaydenlee.recipe.fragment.ConstraintAnimationFragment
import com.example.hexa_jaydenlee.recipe.fragment.CropPhotoFragment
import com.example.hexa_jaydenlee.recipe.fragment.TransitionFragment
import kotlinx.android.synthetic.main.activity_drawer.*
import kotlinx.android.synthetic.main.app_bar_drawer.*

class DrawerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        nav_view.bringToFront()
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_camera -> {
//                    switchFragment(CropPhotoFragment())
                    true
                }
                R.id.nav_gallery -> {
                    switchFragment(TransitionFragment())
                    true
                }
                R.id.nav_slideshow ->{
                    switchFragment( ConstraintAnimationFragment())
                    true
                }
                else -> false
            }
        }

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.drawer, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> {
                switchFragment(CropPhotoFragment())
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    fun switchFragment(frag:Fragment){
        supportFragmentManager
                .beginTransaction()
                .add(R.id.rl_drawer_frame, frag, frag::javaClass.toString())
                .commit()
        drawer_layout.closeDrawer(GravityCompat.START)
    }
}
