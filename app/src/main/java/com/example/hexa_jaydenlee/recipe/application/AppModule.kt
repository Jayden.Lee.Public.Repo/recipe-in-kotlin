package com.example.hexa_jaydenlee.recipe.application

import android.app.Application
import android.content.Context
import com.example.hexa_jaydenlee.recipe.util.AppUtil
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Jayden Lee on 1/23/2018.
 */
@Module class AppModule {

    var mApplication: Application

    constructor(application: Application){ mApplication = application }


    @Provides
    internal fun providesApplication(): Application {
        return mApplication
    }

    @Provides
    @ApplicationContext
    internal fun provideContext(): Context {
        return mApplication
    }

    @Provides
    @Singleton
    fun provideSharePreferenceHelper(): SharePreferenceHelper {
        return SharePreferenceHelper(mApplication.applicationContext)
    }

    @Provides
    @Singleton
    fun provideAppUtil(): AppUtil {
        return AppUtil(mApplication.applicationContext, SharePreferenceHelper(mApplication.applicationContext))
    }
}