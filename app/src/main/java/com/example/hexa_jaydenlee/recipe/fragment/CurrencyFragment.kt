package com.example.hexa_jaydenlee.recipe.fragment

import com.example.hexa_jaydenlee.recipe.base.BaseFragment

/**
 * Created by Jayden Lee on 2/23/2018.
 */
class CurrencyFragment:BaseFragment(){

    override fun injectAppComponent() {
        getAppComponent().inject(this)
    }

}