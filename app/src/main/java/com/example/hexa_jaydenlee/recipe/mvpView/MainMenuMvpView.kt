package com.example.hexa_jaydenlee.recipe.mvpView

import com.example.hexa_jaydenlee.recipe.base.BaseView
import com.example.hexa_jaydenlee.recipe.realm.Recipe

/**
 * Created by  Jayden Lee on 1/12/2018.
 */
interface MainMenuMvpView :BaseView{
    fun showCurrentRecipeValue(recipeType:Int, allRealmResult:List<Recipe>)
}