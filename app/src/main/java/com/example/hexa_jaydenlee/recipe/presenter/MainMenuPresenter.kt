package com.example.hexa_jaydenlee.recipe.presenter

import com.example.hexa_jaydenlee.recipe.base.BasePresenter
import com.example.hexa_jaydenlee.recipe.mvpView.MainMenuMvpView
import com.example.hexa_jaydenlee.recipe.realm.Recipe

/**
 * Created by  Jayden Lee on 1/12/2018.
 */
class MainMenuPresenter: BasePresenter<MainMenuMvpView>(){

    private fun filterRecipe(recipeType:Int) : List<Recipe>{
        return if (recipeType == 0)
            recipeHandler.getAllRecipeWith(realm)
        else
            recipeHandler.getAllRecipeWithFilter(realm, recipeType - 1)

    }

    fun showRecipeListValue(recipeType: Int){
        val allRealmResult: List<Recipe> = filterRecipe(recipeType)
        getMvpView()!!.showCurrentRecipeValue(recipeType, allRealmResult)
    }
}
