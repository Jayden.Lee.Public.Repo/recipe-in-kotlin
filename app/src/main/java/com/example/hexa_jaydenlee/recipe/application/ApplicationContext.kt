package com.example.hexa_jaydenlee.recipe.application

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Qualifier

/**
 * Created by Jayden Lee on 1/23/2018.
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
annotation class ApplicationContext
