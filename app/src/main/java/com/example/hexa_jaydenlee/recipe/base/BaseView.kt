package com.example.hexa_jaydenlee.recipe.base

import android.view.View

/**
 * Created by  Jayden Lee on 1/12/2018.
 */

interface BaseView  {
    fun onAttach()

    fun onDetach()
}