package com.example.hexa_jaydenlee.recipe.realm

import io.realm.Realm
import io.realm.RealmResults

/**
 * Created by  Jayden Lee on 1/5/2018.
 */

class RecipeRealmHandler : RecipeRealmInterface {

    override fun addRecipe(realm: Realm, recipe: Recipe): Boolean {
        try {
            realm.beginTransaction()
            realm.copyToRealmOrUpdate(recipe)
            realm.commitTransaction()
            return true
        } catch (e: Exception) {
            println(e)
            return false
        }
    }

    override fun delRecipe(realm: Realm, recipeID: Int): Boolean {
        try {
            realm.beginTransaction()
            realm.where(Recipe::class.java).equalTo("recipeID", recipeID).findFirst().deleteFromRealm()
            realm.commitTransaction()
            return true
        } catch (e: Exception) {
            println(e)
            return false
        }
    }

    override fun editRecipe(realm: Realm, recipe: Recipe): Boolean {
        try{
            realm.beginTransaction()
            realm.copyToRealmOrUpdate   (recipe)
            realm.commitTransaction()
            return true
        } catch (e : Exception) {
            println(e)
            return false
        }
}

    override fun getRecipe(realm: Realm, recipeID: Int):Recipe {
        return realm.where(Recipe::class.java).equalTo("recipeID", recipeID).findFirst()
    }

    fun getLastRecipeId (realm:Realm) : Recipe{
        return realm.where(Recipe::class.java).findAll().last()
    }

    fun getAllRecipe(realm: Realm) : RealmResults<Recipe>{
        return realm.where(Recipe :: class.java).findAll()
    }

    fun getAllRecipeWith(realm: Realm) : List<Recipe> {
        val realmResult = realm.where(Recipe :: class.java).findAll()
        return realm.copyFromRealm(realmResult)
    }

    fun getAllRecipeWithFilter(realm: Realm , recipeType : Int) : List<Recipe> {
        val realmResult = realm.where(Recipe :: class.java).equalTo("recipeType", recipeType).findAll()
        return realm.copyFromRealm(realmResult)
    }
}