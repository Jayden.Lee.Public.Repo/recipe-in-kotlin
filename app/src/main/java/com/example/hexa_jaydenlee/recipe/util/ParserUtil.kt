package com.example.hexa_jaydenlee.recipe.util

import com.example.hexa_jaydenlee.recipe.model.RecipeTypeModel
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import timber.log.Timber
import java.io.InputStream
import java.util.ArrayList

/**
 * Created by  Jayden Lee on 1/8/2018.
 */

class ParserUtil {
    private var recipeTypeList = ArrayList<RecipeTypeModel>()
    var recipeType = RecipeTypeModel()
    private var name: String = ""

    fun parse(value: InputStream , filterPurpose: Boolean) : ArrayList<RecipeTypeModel>{
        val factory: XmlPullParserFactory
        val parser: XmlPullParser

        try{
            factory = XmlPullParserFactory.newInstance()

            parser = factory.newPullParser()
            parser.setInput(value, null)

            if(filterPurpose){
                val recipeType = RecipeTypeModel()
                recipeType.name= "All"
                recipeType._id = 0
                val returnValue = recipeType.copyRecipe(recipeType)
                recipeTypeList.add(returnValue)
            }
            /*
             Lesson:
             ?:  -> Tenary operator a.k.a Elvis operator,  NPE checker and also acts as and short-form if-else statement

             Ex:
             if (!response.isSuccessful()) {
                    result = "fail"
             } else {
                    result = response.body().string()
             }
             return result

             **TO**

             return (!response.isSuccessful()) ? "fail" : response.body().string()
             */

            var eventType:Int = parser.eventType
            while(eventType != XmlPullParser.END_DOCUMENT){
                val tagName:String = parser.name  ?: ""

                when (eventType) {
                    XmlPullParser.START_TAG -> {
                        if (tagName.equals("recipetype", ignoreCase = true)) {
                            recipeType = RecipeTypeModel()
                            recipeType._id =Integer.parseInt(parser.getAttributeValue(null, "id"))
                        }
                    }

                    XmlPullParser.TEXT ->{
                        name = parser.text
                    }

                    XmlPullParser.END_TAG -> {
                        if (tagName.equals("recipetype", ignoreCase = true)) {
                            recipeTypeList.add(recipeType)
                        } else if (tagName.equals("name", ignoreCase = true)) {
                            recipeType.name = name
                        }
                    }
                }
                eventType = parser.next()
            }
        }catch (e: Exception){
            Timber.e(e)
        }
        return recipeTypeList
    }

}