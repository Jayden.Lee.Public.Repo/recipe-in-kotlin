package com.example.hexa_jaydenlee.recipe.fragment

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.base.BaseFragment
import me.pqpo.smartcropperlib.view.CropImageView
import java.io.File
import android.graphics.BitmapFactory
import timber.log.Timber


/**
 * Created by Jayden Lee on 1/17/2018.
 */
class CropPhotoFragment:BaseFragment(){

    private val REQUEST_TAKE_PHOTO:Int  =  1

    @BindView(R.id.iv_crop) lateinit var ivCrop: CropImageView
    @BindView(R.id.iv_cropped) lateinit var ivCropped: ImageView
    @BindView(R.id.btn_take_photo) lateinit var btnTakePhoto: Button
    lateinit var tempFile: File
    lateinit var selectedBitmap: Bitmap

    companion object {
        fun newInstance() : CropPhotoFragment{
            return CropPhotoFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_crop_photo, container, false)
        ButterKnife.bind(this, view)
        tempFile = File(context.getExternalFilesDir("img"), "temp.jpg")
//        presenter.onAttach(this)
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_TAKE_PHOTO && tempFile.exists()){

            try {

                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = true
                BitmapFactory.decodeFile(tempFile.path, options)
                options.inJustDecodeBounds = false
                options.inSampleSize = calculateSampleSize(options)
                selectedBitmap = BitmapFactory.decodeFile(tempFile.path, options)
                ivCrop.setImageToCrop(selectedBitmap)

            }catch (e:Exception){
                Timber.d(e)
            }

        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun injectAppComponent() {
        getAppComponent().inject(this)
    }

    @OnClick(R.id.btn_take_photo)
    fun takePhoto(){
        ivCrop.visibility = View.VISIBLE
        ivCropped.visibility = View.GONE
        val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile))

        if (cameraIntent.resolveActivity(activity.packageManager) != null) {
            startActivityForResult(cameraIntent, REQUEST_TAKE_PHOTO )
        }
    }

     @OnClick(R.id.btn_crop_photo)
    fun cropPhoto(){
         ivCrop.visibility = View.GONE
         ivCropped.visibility = View.VISIBLE
         val crop = ivCrop.crop()
         ivCropped.setImageBitmap(crop)
     }

    private fun calculateSampleSize(options: BitmapFactory.Options): Int {
        val outHeight = options.outHeight
        val outWidth = options.outWidth
        var sampleSize = 1
        val destHeight = 1000
        val destWidth = 1000
        if (outHeight > destHeight || outWidth > destHeight) {
            if (outHeight > outWidth) {
                sampleSize = outHeight / destHeight
            } else {
                sampleSize = outWidth / destWidth
            }
        }
        if (sampleSize < 1) {
            sampleSize = 1
        }
        return sampleSize
    }
}