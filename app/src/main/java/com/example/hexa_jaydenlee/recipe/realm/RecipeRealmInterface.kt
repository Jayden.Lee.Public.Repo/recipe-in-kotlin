package com.example.hexa_jaydenlee.recipe.realm

import io.realm.Realm

/**
 * Created by  Jayden Lee on 1/5/2018.
 */

interface RecipeRealmInterface {
    fun addRecipe(realm : Realm, recipe: Recipe ) : Boolean
    fun delRecipe(realm : Realm, recipeID : Int) : Boolean
    fun editRecipe(realm: Realm, recipe:Recipe):Boolean
    fun getRecipe(realm: Realm , recipeID:Int): Recipe
}