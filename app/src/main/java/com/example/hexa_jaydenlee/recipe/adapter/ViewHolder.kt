package com.example.hexa_jaydenlee.recipe.adapter

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.fragment.MainMenuMvpFragment
import com.example.hexa_jaydenlee.recipe.fragment.ViewRecipeMvpFragment
import com.example.hexa_jaydenlee.recipe.realm.Recipe

/**
 * Created by  Jayden Lee on 1/9/2018.
 */
class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    //bind view with the  card view
    fun bindItems(value: Recipe) {
        val tvRecipeName = itemView.findViewById(R.id.tv_recipe_name) as TextView
        tvRecipeName.text = value.recipeName
        itemView.setOnClickListener {
            val viewRecipe = ViewRecipeMvpFragment.newInstance(value.recipeID)

            val activity = itemView.context as AppCompatActivity
            activity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frm_layout, viewRecipe)
                    .addToBackStack(MainMenuMvpFragment::javaClass.toString())
                    .commit()
        }
    }
}