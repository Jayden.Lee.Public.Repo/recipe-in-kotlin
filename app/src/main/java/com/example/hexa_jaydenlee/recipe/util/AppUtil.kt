package com.example.hexa_jaydenlee.recipe.util

import android.content.Context
import com.example.hexa_jaydenlee.recipe.application.ApplicationContext
import com.example.hexa_jaydenlee.recipe.application.SharePreferenceHelper
import javax.inject.Inject

/**
 * Created by Jayden Lee on 1/23/2018.
 */
class AppUtil{

    private val mContext: Context
    private val sharePreferenceHelper:SharePreferenceHelper

    @Inject
    constructor(@ApplicationContext mContext:Context , sharePreferenceHelper: SharePreferenceHelper){
        this.mContext = mContext
        this.sharePreferenceHelper= sharePreferenceHelper
    }


}