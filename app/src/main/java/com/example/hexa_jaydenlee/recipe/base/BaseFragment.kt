package com.example.hexa_jaydenlee.recipe.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.application.App
import com.example.hexa_jaydenlee.recipe.application.AppComponent
import com.example.hexa_jaydenlee.recipe.fragment.MainMenuMvpFragment
import com.example.hexa_jaydenlee.recipe.realm.RecipeRealmHandler
import io.realm.Realm

/**
 * Created by  Jayden Lee on 1/9/2018.
 */
abstract class BaseFragment :Fragment(){

    //Initialize Realm
    var recipeHandler = RecipeRealmHandler()
    var realm = Realm.getDefaultInstance()!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectAppComponent()
    }

    fun toastValue(value:String){
        Toast.makeText(activity, value , Toast.LENGTH_SHORT).show()
    }

    fun switchFragment(frag : Fragment){
        fragmentManager
                .beginTransaction()
                .replace(R.id.frm_layout, frag)
                .addToBackStack(MainMenuMvpFragment::javaClass.toString())
                .commit()
    }



    fun backToPreviousFrag(){
        activity.supportFragmentManager.popBackStack()
    }

    protected abstract fun injectAppComponent()

    fun getAppComponent(): AppComponent {
        return App.getAppComponent(activity)
    }
}