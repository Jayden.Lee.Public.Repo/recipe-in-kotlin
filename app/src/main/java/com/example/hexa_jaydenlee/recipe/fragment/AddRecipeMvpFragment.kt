package com.example.hexa_jaydenlee.recipe.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.hexa_jaydenlee.recipe.base.BaseFragment
import com.example.hexa_jaydenlee.recipe.R
import com.example.hexa_jaydenlee.recipe.model.RecipeTypeModel
import com.example.hexa_jaydenlee.recipe.mvpView.AddRecipeMvpView
import com.example.hexa_jaydenlee.recipe.presenter.AddRecipePresenter
import com.example.hexa_jaydenlee.recipe.util.AppUtil
import com.example.hexa_jaydenlee.recipe.util.ParserUtil
import javax.inject.Inject

/**
 * Created by  Jayden Lee on 1/5/2018.
 */

class AddRecipeMvpFragment : BaseFragment(), AddRecipeMvpView {

    @Inject lateinit var mAppUtil: AppUtil
    override fun onAttach() {
    }

    override fun showFailedAdd() {
    }

    override fun injectAppComponent() {
        getAppComponent().inject(this)
    }

    private val presenter: AddRecipePresenter = AddRecipePresenter()
    //Declaration
    @BindView(R.id.btn_add_recipe) lateinit var btnAddRecipe: Button
    @BindView(R.id.et_recipe_name) lateinit var etRecipeName: EditText
    @BindView(R.id.et_recipe_ingredient) lateinit var etRecipeIngredient: EditText
    @BindView(R.id.spinner1) lateinit var  spnRecipeType : Spinner

    companion object {
        fun newInstance() : AddRecipeMvpFragment {
            return AddRecipeMvpFragment()
        }
    }

    /*
    Lesson:
    !!  -> If the value is null , will throw NPE
    ?   -> If the value is null, will show null value without throw NPE
    */
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_add_recipe, container, false)
        ButterKnife.bind(this, view)
        presenter.onAttach(this)
        // Parse the XML value into Spinner
        val parser= ParserUtil()
        val recipeTypeList:List<RecipeTypeModel> = parser.parse(resources.openRawResource(R.raw.recipetypes),false)
        val spinnerAdapter: ArrayAdapter<RecipeTypeModel> = ArrayAdapter(activity, R.layout.support_simple_spinner_dropdown_item, recipeTypeList)
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spnRecipeType.adapter = spinnerAdapter

        return view
    }

    override fun onResume() {
        super.onResume()
        spnRecipeType.setSelection(0)
    }

    @OnClick(R.id.btn_add_recipe)
    fun addRecipe(){

        if(etRecipeName.text.toString() == "" && etRecipeIngredient.text.toString() == ""){
            toastValue(resources.getString(R.string.toast_fill_field))
        }else {
            presenter.addRecipe(etRecipeName.text.toString(), etRecipeIngredient.text.toString(), spnRecipeType.selectedItemPosition)
        }
    }

    override fun showSuccessAdd() {
        toastValue(resources.getString(R.string.toast_recipe_added))
        backToPreviousFrag()
    }


    // DIsplay data in Log
//    fun displayResult(realm: Realm) {
//        var data = ""
//        var results = recipeHandler.getAllRecipe(realm)
//        results.forEach { result ->
//            data += "$result \n --------------------------------------- \n"
//        }
//        Log.d("Result" , data)
//    }
}